import WSHandler from "./handler"

export const wsMiddleware = ({ _getState }) => {
  const wsHandler = new WSHandler()

  return (next) => (action) => {
    switch (action.type) {
      case "WS_REGISTER":
        wsHandler.register()
        break
      case "WS_UNREGISTER":
        wsHandler.unregister()
        break
      case "WS_SEND_MESSAGE":
        wsHandler.sendMessage(action.payload.message)
        break
      case "WS_REGISTER_MESSAGE_LOGGER":
        wsHandler.registerMessageLogger(action.payload.logger)
        break
    }
    return next(action)
  }
}
