import { store } from "../store/index"

export default class WsHandler {
  constructor() {
    this.socket = null
    this.messageLogger = null
  }

  register() {
    // should be set through env
    this.socket = new WebSocket("ws://localhost:8001")
    this.socket.addEventListener("message", (event) => {
      this.receiveMessage(event)
    })
  }

  unregister() {
    this.socket.close()
    this.socket = null
    // should be separate? mungkin?
    this.messageLogger = null
  }

  sendMessage(message) {
    let msgData = {
      Message: {
        username: store.getState().username,
        avatarUrl: store.getState().avatarUrl,
        message: message,
      },
    }
    this.socket.send(JSON.stringify(msgData))
    if (this.messageLogger) {
      this.messageLogger(msgData)
    }
  }

  registerMessageLogger(logger) {
    this.messageLogger = logger
  }

  receiveMessage(event) {
    let eventData
    try {
      eventData = JSON.parse(event.data)
    } catch {
      console.log("failed to parse message json")
      return
    }
    let eventDataKeys = Object.keys(eventData)

    // this whole thing could be a lot better
    switch (eventDataKeys[0]) {
      case "Message":
        if (this.messageLogger) {
          this.messageLogger(eventData)
        }
        break
      default:
        return
    }
  }
}
