import * as actions from "./action-types"

export function wsSendMessage(message) {
  return {
    type: actions.WS_SEND_MESSAGE,
    payload: {
      message: message,
    },
  }
}

export function wsRegister() {
  return {
    type: actions.WS_REGISTER,
  }
}

export function wsUnregister() {
  return {
    type: actions.WS_UNREGISTER,
  }
}

export function wsRegisterMessageLogger(logger) {
  return {
    type: actions.WS_REGISTER_MESSAGE_LOGGER,
    payload: {
      logger: logger,
    },
  }
}
