import axios from "axios"

export function requestSession(cb) {
  axios
    .get("/api/requestSession")
    .then((resp) => {
      window.location.href = resp.data.url
    })
    .catch((err) => cb(false, err))
}

export function loginRequest(token, cb) {
  axios
    .post("/api/login", {
      token: token,
    })
    .then((resp) => {
      cb(true, resp.data)
    })
    .catch((err) => cb(false, null, err))
}
