import storage from "redux-persist/lib/storage"

const coreState = {
  loggedIn: false,
  username: null,
  token: null,
  avatarUrl: null,
}

export default function reducer(state = coreState, action) {
  switch (action.type) {
    case "LOGIN":
      return {
        ...state,
        loggedIn: true,
        username: action.payload.username,
        token: action.payload.token,
        avatarUrl: action.payload.avatarUrl,
      }
    case "LOGOUT":
      return {
        ...state,
        loggedIn: false,
        username: null,
        token: null,
        avatarUrl: null,
      }
    case "HARD_RESET":
      storage.removeItem("persist:root")
      state = undefined
      break
    default:
      return state
  }
}
