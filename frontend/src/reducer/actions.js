import * as actions from "./action-types"

export function login(username, token, avatarUrl) {
  return {
    type: actions.LOGIN,
    payload: {
      username: username,
      token: token,
      avatarUrl: avatarUrl,
    },
  }
}

export function logout() {
  return {
    type: actions.LOGOUT,
  }
}
