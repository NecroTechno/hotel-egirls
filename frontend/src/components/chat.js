import React from "react"
import { connect } from "react-redux"
import { wsRegisterMessageLogger } from "../websocket/actions"
import "./chat.css"

const mapStateToProps = (_state) => {
  return {}
}

const mapDispatchToProps = { wsRegisterMessageLogger }

class Chat extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      log: [],
    }
  }

  pushToLog = (newMesg) => {
    this.setState(
      (state) => {
        let newLog = state.log.concat([newMesg.Message])
        console.log(newLog)
        return {
          ...state,
          log: newLog,
        }
      },
      () => {
        setTimeout(this.shiftLog, 10000)
      }
    )
  }

  shiftLog = () => {
    this.setState((state) => {
      let newLog = state.log.slice(1)
      return {
        ...state,
        log: newLog,
      }
    })
  }

  renderLog = () => {
    return this.state.log.map((msg, i) => (
      <div key={i} className="chat__box">
        <div>
          <img className="chat__avatar" src={msg.avatarUrl} />
        </div>
        <div>
          <p className="chat__text">
            <span className="chat__name">{msg.username}: </span>
            {msg.message}
          </p>
        </div>
      </div>
    ))
  }

  componentDidMount() {
    this.props.wsRegisterMessageLogger(this.pushToLog)
  }

  render() {
    return (
      <div className="chat__container">{this.renderLog()}</div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
