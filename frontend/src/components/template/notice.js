import React from "react"
import { connect } from "react-redux"
import { login } from "../../reducer/actions"

const mapStateToProps = (_state) => {
  return {}
}

const mapDispatchToProps = { login }

class Notice extends React.Component {
  testLogin = () => {
    this.props.login(
      "test_user",
      "abcde",
      "https://egirls.gay/files/thumbnail-7af5e0fc-d6eb-4a52-ace1-2427bc326e99"
    )
  }

  render() {
    return (
      <main role="main" className="flex-shrink-0">
        <div className="container notice">
          <p>
            This is an early test build and will probably immediately collapse
            upon any user interaction.
          </p>
          <br />
          <p>This software is not affiliated in any way with Habbo...</p>
          <br />
          <p>
            <span onClick={this.testLogin}>PSYCHE!</span> Fuck IP law.
          </p>
        </div>
      </main>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Notice)
