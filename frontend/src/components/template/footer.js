import React from "react"
import E from "../../assets/font/e.png"
import { connect } from "react-redux"
import { wsSendMessage } from "../../websocket/actions"

const mapStateToProps = (state) => {
  return {
    loggedIn: state.loggedIn,
    avatarUrl: state.avatarUrl,
  }
}

const mapDispatchToProps = { wsSendMessage }

class Footer extends React.Component {
  constructor(props) {
    super(props)

    this.messageBox = React.createRef()
  }

  componentDidMount() {
    this.messageBox.current.addEventListener("keydown", (e) => {
      var code = e.keyCode ? e.keyCode : e.which
      if (code === 13) {
        this.props.wsSendMessage(this.messageBox.current.value)
        this.messageBox.current.value = ""
      }
    })
  }

  render() {
    return (
      <footer className="footer mt-auto py-3">
        <span className="left_section">
          <img src={E} alt="E" className="font" height="37" />
        </span>
        <span className="middle_section">
          <button>
            <img src={this.props.avatarUrl} height="37" alt="Me" />
          </button>
          <input
            type="text"
            ref={this.messageBox}
            maxLength="95"
            name="chat"
            id="chat"
            autoComplete="off"
            placeholder="Begin a chat..."
          />
        </span>
      </footer>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer)
