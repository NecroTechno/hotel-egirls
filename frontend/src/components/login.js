import React from "react"
import "./login.css"
import { connect } from "react-redux"
import { login } from "../reducer/actions"
import { requestSession, loginRequest } from "../requests"
import { Ellipsis } from "react-css-spinners"

const mapStateToProps = (_state) => {
  return {}
}

const mapDispatchToProps = { login }

class Login extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      canLogin: true,
    }
  }

  redirectToLogin = () => {
    this.setState(
      (state) => {
        return {
          ...state,
          canLogin: false,
        }
      },
      () => {
        requestSession((_success, err) => {
          alert(err)
        })
      }
    )
  }

  componentDidMount() {
    let qsParams = new URLSearchParams(window.location.search)
    if (qsParams.has("token")) {
      let token = qsParams.get("token")
      console.log(token)
      window.history.replaceState({}, document.title, "/")
      this.setState(
        (state) => {
          return {
            ...state,
            canLogin: false,
          }
        },
        () => {
          loginRequest(token, (success, data, err = null) => {
            if (success) {
              this.props.login(data.username, data.token, data.avatarUrl)
            } else {
              alert(err)
            }
          })
        }
      )
    }
  }

  render() {
    return (
      <div id="loginform">
        <div className="dialog">
          <h2 className="title">Login</h2>
          <div className="wrapper">
            <p>Login with your egirls.gay account to start playing.</p>
            {this.state.canLogin ? (
              <button id="login" onClick={this.redirectToLogin}>
                Login
              </button>
            ) : (
              <Ellipsis color="#ff3737" />
            )}
            <br />
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
