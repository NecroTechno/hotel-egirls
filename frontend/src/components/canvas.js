import React from "react"
import * as PIXI from "pixi.js"
import "./client.css"
import "./login.css"
import { connect } from "react-redux"
import { wsRegister, wsUnregister } from "../websocket/actions"
import { render } from "../pixi/render"

const mapStateToProps = (state) => {
  return {
    loggedIn: state.loggedIn,
    username: state.username,
    token: state.token,
    avatarUrl: state.avatarUrl,
  }
}

const mapDispatchToProps = { wsRegister, wsUnregister }

class Canvas extends React.Component {
  constructor(props) {
    super(props)

    this.app = new PIXI.Application({
      backgroundColor: 0x000000,
      width: window.innerWidth,
      height: window.innerHeight,
    })
    this.canvas = React.createRef()
  }

  componentWillUnmount() {
    this.props.wsUnregister()
  }

  componentDidMount() {
    this.props.wsRegister()
    this.app.view.style.display = "block"
    this.canvas.current.appendChild(this.app.view)
    render(this.app)
    this.app.start()

    //debug
    window.app = this.app
  }

  render() {
    return (
      <React.Fragment>
        <div ref={this.canvas} id="canvas"></div>
      </React.Fragment>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Canvas)
