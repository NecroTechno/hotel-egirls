import React from "react"
import "./client.css"
import "./login.css"
import Login from "./login"
import Notice from "./template/notice"
import TopBar from "./template/topbar"
import Footer from "./template/footer"
import Chat from "./chat.js"
import { connect } from "react-redux"

import Canvas from "./canvas"

const mapStateToProps = (state) => {
  return {
    loggedIn: state.loggedIn,
  }
}

class Client extends React.Component {
  render() {
    let mainStyle = {
      //background: `url(${Hotel}) no-repeat top center`,
      width: "100%",
      height: "100%",
      margin: 0,
      padding: 0,
    }

    let loggedIn = this.props.loggedIn

    return (
      <div id="main" style={loggedIn ? {} : mainStyle}>
        {loggedIn ? (
          <React.Fragment>
            <TopBar />
            <Chat />
            <Canvas />
            <Footer />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Login />
            <Notice />
          </React.Fragment>
        )}
      </div>
    )
  }
}

export default connect(mapStateToProps)(Client)
