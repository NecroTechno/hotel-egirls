import * as PIXI from "pixi.js"

export function render(app) {
  const container = new PIXI.Container()
  const tiles = [
    [1, 0],
    [0, 0],
  ]

  tiles.sort().forEach((tile) => {
    const roomTile = new RoomTile(tile)

    container.addChild(roomTile)
  })

  container.position.set(
    app.renderer.view.width / 2,
    app.renderer.view.height / 2
  )
  console.log(container.position)

  container.interactive = true
  container.buttonMode = true
  container
    .on("pointerdown", onDragStart)
    .on("pointerup", onDragEnd)
    .on("pointerupoutside", onDragEnd)
    .on("pointermove", onDragMove)

  app.stage.addChild(container)
}

function onDragStart(event) {
  // store a reference to the data
  // the reason for this is because of multitouch
  // we want to track the movement of this particular touch
  this.data = event.data
  this.dragging = true
}

function onDragEnd() {
  this.dragging = false
  // set the interaction data to null
  this.data = null
}

function onDragMove(_container) {
  if (this.dragging) {
    const newPosition = this.data.getLocalPosition(this.parent)
    this.x = newPosition.x
    this.y = newPosition.y
  }
}

class RoomTile extends PIXI.Graphics {
  constructor(tile) {
    super()

    this.interactive = true
    this.points = []
    this.pos = {
      x: tile[0],
      y: tile[1],
      z: tile[2], //default floor height is 0
    }

    this.drawStandardTile()
  }

  resetPoints() {
    this.points = []
  }

  drawStandardTile() {
    const floorThick = 12
    this.resetPoints()

    const width = 64
    const height = 32

    const startY = this.pos.y * 16 + this.pos.x * 16 - (this.pos.z * 16 || 0)
    const startX = this.pos.y * 32 - this.pos.x * 32 // * (tile[2] || 1)

    this.points.push({ x: startX, y: startY })
    this.points.push({ x: startX - width / 2, y: startY + height / 2 })
    this.points.push({ x: startX, y: startY + height })
    this.points.push({ x: startX + width / 2, y: startY + height / 2 })

    this.hitArea = new PIXI.Polygon(
      this.points.map(({ x, y }) => {
        return new PIXI.Point(x, y)
      })
    )

    this.beginFill(0x989865)
    this.lineStyle(0.5, 0x8e8e5e) //2, 0x8E8E5E
    this.moveTo(this.points[0].x, this.points[0].y)
    this.lineTo(this.points[1].x, this.points[1].y)
    this.lineTo(this.points[2].x, this.points[2].y)
    this.lineTo(this.points[3].x, this.points[3].y)
    this.lineTo(this.points[0].x, this.points[0].y)

    this.endFill()

    if (floorThick > 0) {
      this.beginFill(0x838357)
      this.lineStyle(1, 0x7a7a51)
      this.moveTo(this.points[1].x - 0.5, this.points[1].y)
      this.lineTo(this.points[1].x - 0.5, this.points[1].y + floorThick)
      this.lineTo(this.points[2].x - 0.5, this.points[2].y + floorThick)
      this.lineTo(this.points[2].x - 0.5, this.points[2].y)
      this.endFill()

      this.beginFill(0x6f6f49)
      this.lineStyle(1, 0x676744)
      this.moveTo(this.points[3].x + 0.5, this.points[3].y)
      this.lineTo(this.points[3].x + 0.5, this.points[3].y + floorThick)
      this.lineTo(this.points[2].x + 0.5, this.points[2].y + floorThick)
      this.lineTo(this.points[2].x + 0.5, this.points[2].y)
      this.endFill()
    }
  }

  click = () => console.log(this.pos)
}
