import { createStore, applyMiddleware } from "redux"
import reducer from "../reducer/"
import { persistStore, persistReducer } from "redux-persist"
import storage from "redux-persist/lib/storage"
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2"
import { encryptTransform } from "redux-persist-transform-encrypt"
import { wsMiddleware } from "../websocket/middleware"

const encryptor = encryptTransform({
  // TODO: Move into CI
  secretKey: "0F%v9oadlvcY",
  onError: (err) => {
    console.log("encrypt error: ", err)
  },
})

const persistConfig = {
  key: "root",
  storage: storage,
  stateReconciler: autoMergeLevel2,
  transforms: [encryptor],
}

const pReducer = persistReducer(persistConfig, reducer)

const middleware = applyMiddleware(wsMiddleware)
export const store = createStore(pReducer, middleware)
export const persistor = persistStore(store)
