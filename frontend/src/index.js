import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import Client from "./components/client"
import * as serviceWorker from "./serviceWorker"
import "bootstrap/dist/css/bootstrap.min.css"
import { Provider } from "react-redux"
import { persistor, store } from "./store/index"
import { PersistGate } from "redux-persist/lib/integration/react"

// TODO: make sure this is not available outside of development env
window.store = store

const onBeforeLift = () => {
  // Uncomment below to clear store on reload
  // store.dispatch({type: "HARD_RESET"})
}

ReactDOM.render(
  <Provider store={store}>
    <PersistGate onBeforeLift={onBeforeLift} persistor={persistor}>
      <Client />
    </PersistGate>
  </Provider>,
  document.getElementById("root")
)

serviceWorker.unregister()
