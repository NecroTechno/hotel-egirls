CREATE TABLE users (
      id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      username VARCHAR NOT NULL,
      secret VARCHAR NOT NULL,
      avatar_url VARCHAR
);

INSERT INTO users ('username', 'secret') VALUES ('test_user', 'test_secret');
