#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate dotenv_codegen;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
extern crate bcrypt;
extern crate reqwest;

mod db;
mod types;
mod websocket;

use std::collections::HashMap;
use std::io;
use std::path::{Path, PathBuf};
use std::thread;

use bcrypt::{hash, verify, DEFAULT_COST};
use reqwest::Client;
use rocket::response::NamedFile;
use rocket_contrib::json;
use rocket_contrib::json::Json;

use db::interface::{create_user, get_user_by_username, run_migrations};
use types::{ApiOk, ApiResponse, AuthSessionGenerateResponse, LoginData, UserKeyResponse};
use websocket::ws_manager;

#[get("/requestSession")]
fn request_session() -> ApiResponse {
    let extern_request_client = Client::new();
    let mut generate_session_body = HashMap::new();
    generate_session_body.insert("appSecret", dotenv!("APPSECRET"));

    let future = async move {
        extern_request_client
            .post(format!("{}{}", dotenv!("API_URL"), "auth/session/generate"))
            .json(&generate_session_body)
            .send()
            .await?
            .json::<AuthSessionGenerateResponse>()
            .await
    };

    let runtime_as_result = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build();

    let runtime = match runtime_as_result {
        Ok(val) => val,
        Err(err) => return ApiResponse::e_503(Some(format!("{}", err))),
    };

    // required until rocket handles async
    match runtime.block_on(future) {
        Ok(val) => return ApiResponse::ok(ApiOk::Json(json!(val))),
        Err(err) => return ApiResponse::e_503(Some(format!("{}", err))),
    }
}

#[post("/login", data = "<login_data>")]
fn login(login_data: Json<LoginData>) -> ApiResponse {
    let extern_request_client = Client::new();
    let mut generate_session_body = HashMap::new();
    generate_session_body.insert("appSecret", dotenv!("APPSECRET"));
    generate_session_body.insert("token", &login_data.token);

    let future = async move {
        let res = extern_request_client
            .post(format!("{}{}", dotenv!("API_URL"), "auth/session/userkey"))
            .json(&generate_session_body)
            .send()
            .await?;
        res.json::<UserKeyResponse>().await
    };

    let runtime_as_result = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build();

    let runtime = match runtime_as_result {
        Ok(val) => val,
        Err(err) => return ApiResponse::e_503(Some(format!("{}", err))),
    };

    // required until rocket handles async
    let user_data = match runtime.block_on(future) {
        Ok(val) => val,
        Err(err) => return ApiResponse::e_503(Some(format!("{}", err))),
    };

    let current_user = get_user_by_username(user_data.user.username.clone());
    let hash_result = hash(
        format!("{}{}", dotenv!("APPSECRET"), user_data.user.id),
        DEFAULT_COST,
    );

    let hash = match hash_result {
        Ok(val) => val,
        Err(err) => return ApiResponse::e_503(Some(format!("{}", err))),
    };

    if current_user.is_empty() {
        create_user(
            user_data.user.username.clone(),
            hash,
            user_data.user.avatar_url.clone(),
        );
    } else if !verify(
        format!("{}{}", dotenv!("APPSECRET"), user_data.user.id),
        &current_user[0].secret,
    )
    .unwrap_or(false)
    {
        println!("{:?}", current_user[0]);
        return ApiResponse::e_503(Some("Secret mismatch".to_string()));
    }

    ApiResponse::ok(ApiOk::Json(
        json!({"username": user_data.user.username, "token": login_data.token, "avatarUrl": user_data.user.avatar_url}),
    ))
}

#[get("/<file..>")]
fn files(file: PathBuf) -> io::Result<NamedFile> {
    let page_directory_path = format!("{}/../frontend/build", env!("CARGO_MANIFEST_DIR"));
    let full_path = Path::new(&page_directory_path).join(file);
    println!("{:?}", full_path);
    NamedFile::open(full_path)
}

#[get("/")]
fn index() -> io::Result<NamedFile> {
    let page_directory_path = format!("{}/../frontend/build", env!("CARGO_MANIFEST_DIR"));
    NamedFile::open(Path::new(&page_directory_path).join("index.html"))
}

fn main() {
    run_migrations();

    thread::spawn(|| {
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .unwrap();
        rt.block_on(async move { ws_manager().await }).expect("websocket got fucked up");
    });

    rocket::ignite()
        .mount("/", routes![index, files])
        .mount("/api", routes![request_session, login])
        .launch();
}
