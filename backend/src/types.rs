use rocket::http::{ContentType, Status};
use rocket::request::Request;
use rocket::response;
use rocket::response::{Responder, Response};
use rocket_contrib::json;
use rocket_contrib::json::JsonValue;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub enum WsType {
    Message {
        username: String,
        message: String,
        #[serde(rename(deserialize = "avatarUrl"))]
        avatar_url: String,
    },
}

#[derive(Deserialize, Serialize)]
pub struct LoginData {
    pub token: String,
}

#[derive(Deserialize, Serialize)]
pub struct UserKeyResponse {
    #[serde(rename(deserialize = "accessToken"))]
    pub access_token: Option<String>,
    pub user: UserKeyUser,
}

#[derive(Deserialize, Serialize)]
pub struct UserKeyUser {
    pub id: String,
    pub name: String,
    pub username: String,
    #[serde(rename(deserialize = "avatarUrl"))]
    pub avatar_url: Option<String>,
}

#[derive(Deserialize, Serialize)]
pub struct AuthSessionGenerateResponse {
    token: String,
    url: String,
}

pub enum ApiOk<'a> {
    Message(&'a str),
    Json(JsonValue),
}

#[derive(Debug)]
pub struct ApiResponse {
    json: JsonValue,
    status: Status,
}

impl ApiResponse {
    pub fn ok(ok_ret: ApiOk) -> ApiResponse {
        let return_value = match ok_ret {
            ApiOk::Message(message_inner) => json!({ "message": message_inner }),
            ApiOk::Json(json_inner) => json_inner,
        };
        ApiResponse {
            json: return_value,
            status: Status::Ok,
        }
    }

    pub fn e_503(err_ret: Option<String>) -> ApiResponse {
        let return_value = match err_ret {
            Some(val) => json!({ "message": val }),
            None => json!({"message": "503 Service Unavailable"}),
        };
        ApiResponse {
            json: return_value,
            status: Status::ServiceUnavailable,
        }
    }
}

impl<'r> Responder<'r> for ApiResponse {
    fn respond_to(self, req: &Request) -> response::Result<'r> {
        Response::build_from(self.json.respond_to(&req).unwrap())
            .status(self.status)
            .header(ContentType::JSON)
            .ok()
    }
}
