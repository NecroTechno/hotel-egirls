table! {
    users (id) {
        id -> Integer,
        username -> Text,
        secret -> Text,
        avatar_url -> Nullable<Text>,
    }
}
