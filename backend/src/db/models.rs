use super::schema::users;
use serde::Serialize;

#[derive(Serialize, Queryable, Debug)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub secret: String,
    pub avatar_url: Option<String>,
}

#[derive(Insertable)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub username: &'a str,
    pub secret: &'a str,
    pub avatar_url: Option<String>,
}
