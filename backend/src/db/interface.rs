use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use diesel_migrations::embed_migrations;
use dotenv::dotenv;
use std::env;

use super::models::*;
use super::schema::users;
use super::schema::users::dsl::*;

fn establish_connection() -> SqliteConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub fn run_migrations() {
    embed_migrations!();
    let connection = establish_connection();
    embedded_migrations::run(&connection).expect("migrations got fucked up");
}

pub fn get_users() -> Vec<User> {
    let connection = establish_connection();
    users
        .load::<User>(&connection)
        .expect("Error fetching users")
}

pub fn get_user_by_username(search_username: String) -> Vec<User> {
    let connection = establish_connection();
    users
        .filter(username.eq(search_username))
        .load::<User>(&connection)
        .expect("Error fetching users")
}

pub fn create_user(new_username: String, new_secret: String, new_avatar_url: Option<String>) {
    let connection = establish_connection();

    let new_user = NewUser {
        username: &new_username,
        secret: &new_secret,
        avatar_url: new_avatar_url,
    };

    diesel::insert_into(users::table)
        .values(&new_user)
        .execute(&connection)
        .expect("Error saving new user");
}
